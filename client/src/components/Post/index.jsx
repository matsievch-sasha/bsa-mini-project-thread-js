import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Confirm } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ userId, post, likePost, dislikePost,
  toggleExpandedPost, sharePost, toggleUpdatedPost, deletePost }) => {
  const [showConfirm, setshowConfirm] = useState(false);
  const {
    id,
    userId: userPostId,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const handleCancel = () => setshowConfirm(false);
  const handleConfirm = () => {
    setshowConfirm(false);
    deletePost(id);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {(userId === userPostId)
          && (
            <div className={styles.wrappedReductTrash}>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => toggleUpdatedPost(id)}
              >
                <Icon name="pencil alternate" />
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => setshowConfirm(true)}
              >
                <Icon name="trash alternate outline" />
              </Label>
              <Confirm open={showConfirm} onCancel={handleCancel} onConfirm={handleConfirm} />
            </div>
          )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  toggleUpdatedPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
