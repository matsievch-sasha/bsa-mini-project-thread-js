import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { toggleUpdatedPost, updatePost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';
import UpdatePost from 'src/components/UpdatePost';

const ModalUpdatePost = ({
  post,
  uploadImage,
  toggleUpdatedPost: toggle,
  updatePost: update
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <UpdatePost post={post} updatePost={update} uploadImage={uploadImage} toggleUpdatedPost={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ModalUpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleUpdatedPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.updatedPost
});

const actions = { toggleUpdatedPost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalUpdatePost);
