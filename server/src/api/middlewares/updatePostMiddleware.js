import * as postService from '../services/postService';

export default (req, res, next) => {
  postService.getPostById(req.params.postId)
    .then(post => {
      if (req.user.id === post.userId) {
        next();
      } else {
        res.status(400).send('Unauthorized');
      }
    });
};
