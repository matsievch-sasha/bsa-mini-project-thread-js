import { Router } from 'express';
import * as postService from '../services/postService';
import updatePostMiddleware from '../middlewares/updatePostMiddleware';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .get('/react/:postId', (req, res, next) => postService.getReaction(req.user.id, req.params.postId)
    .then(reaction => res.send(reaction))
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.isLike === true && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        const message = 'Your post was liked!';
        req.io.to(reaction.post.userId).emit('like', message);
      } else if (reaction.isLike === false && (reaction.post.userId !== req.user.id)) {
        const message = 'Your post was disliked!';
        req.io.to(reaction.post.userId).emit('like', message);
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:postId', updatePostMiddleware, (req, res, next) => postService.updatePost(req.params.postId, req.body)
    .then(post => {
      req.io.emit('update_post', post);
      return res.send(post);
    })
    .catch(next))
  .delete('/:postId', updatePostMiddleware, (req, res, next) => postService.deleteSoft(req.params.postId)
    .then(result => res.send(result))
    .catch(next));
export default router;
