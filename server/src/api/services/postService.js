import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = (reaction)
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });
  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const getReaction = async (userId, postId) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  let result;
  if (reaction) {
    const { isLike: prevIsLike } = reaction;
    result = { prevIsLike };
  } else result = {};
  return result;
};

export const updatePost = async (postId, body) => postRepository.updateById(postId, body);

export const deleteSoft = async postId => {
  const result = await postRepository.deleteById(postId);
  return Number.isInteger(result) ? { postId } : { error: 'something went wrong' };
};
